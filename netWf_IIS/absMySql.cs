﻿using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web;

// absMySql 
//
// absMySql MySQL Implementation Class
//
// Copyright 2015 Jorge Alberto Ponce Turrubiates
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace netWf_IIS
{
    /// <summary>
    /// MySQL Implementation Class
    /// </summary>
    public class absMySql : absLayer
    {
        /// <summary>
        /// Logger Object.
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(absMySql));

        /// <summary>
        /// MySQL Connection
        /// </summary>
        MySqlConnection conn = null;

        /// <summary>
        /// Constructor of class
        /// </summary>
        public absMySql()
        {
            conn = new MySqlConnection();

            conn.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["netdb_connection"].ToString();

            try
            {
                conn.Open();

                if (!(conn.State == ConnectionState.Open))
                {
                    conn = null;
                }
            }
            catch (Exception e)
            {
                logger.Error(e.Message);

                conn = null;
            }
        }

        /// <summary>
        /// Checks if Connected to MySQL Server
        /// </summary>
        /// <returns>
        /// bool
        /// </returns>
        public override bool IsConnected()
        {
            return conn == null ? false : true;
        }

        /// <summary>
        /// Execute query without result
        /// </summary>
        /// <param name="query">
        /// Query to execute
        /// </param>
        /// <param name="p">
        /// Parameters to query
        /// </param>
        /// <returns>
        /// -2 Not Connected -1 SQL Exception 0 No Results in execution >0 Rows Affected
        /// </returns>
        public override int ExecuteNonQuery(string query, params DbParameter[] p)
        {
            int retValue = 0;

            MySqlConnection connection = getConnection();

            if (IsConnected())
            {
                try
                {
                    MySqlCommand myCommand = new MySqlCommand(query, connection);

                    for (var i = 0; i < p.Length; i++)
                    {
                        myCommand.Parameters.Add(p[i]);
                    }

                    retValue = myCommand.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    logger.Error(e.Message);

                    retValue = -1;
                }
            }
            else
                retValue = -2;

            connection.Close();
            connection = null;

            return retValue;
        }

        /// <summary>
        /// Execute query with result
        /// </summary>
        /// <param name="query">
        /// Query to execute
        /// </param>
        /// <param name="p">
        /// Parameters to query
        /// </param>
        /// <returns>
        /// Dataset
        /// </returns>
        public override DataSet Execute(string query, params DbParameter[] p)
        {
            MySqlConnection connection = getConnection();
            DataSet myDs = null;

            if (IsConnected())
            {
                try
                {
                    MySqlCommand myCommand = new MySqlCommand(query, connection);
                    MySqlDataAdapter myAdapter = new MySqlDataAdapter();
                    myDs = new DataSet();

                    for (var i = 0; i < p.Length; i++)
                    {
                        myCommand.Parameters.Add(p[i]);
                    }

                    myAdapter.SelectCommand = myCommand;
                    myAdapter.Fill(myDs);
                }
                catch (Exception e)
                {
                    logger.Error(e.Message);

                    myDs = new DataSet();
                    myDs.Tables.Add(GetErrorTable(-1, e.Message));
                }
            }
            else
            {
                myDs = new DataSet();
                myDs.Tables.Add(GetErrorTable(-2, "Not Connected to MySQL"));
            }

            connection.Close();
            connection = null;

            return myDs;
        }

        /// <summary>
        /// Returns a MySQL Parameter
        /// </summary>
        /// <param name="name">
        /// Parameter Name
        /// </param>
        /// <param name="value">
        /// Parameter Value
        /// </param>
        /// <returns>
        /// DbParameter
        /// </returns>
        public override DbParameter GetParameter(string name, object value)
        {
            return new MySqlParameter(name, value);
        }

        /// <summary>
        /// Returns First Row of Table
        /// </summary>
        /// <param name="table">
        /// Table Name
        /// </param>
        /// <param name="fieldKey">
        /// Field Key
        /// </param>
        /// <param name="key">
        /// Field Key Value
        /// </param>
        /// <param name="where">
        /// Where Statement
        /// </param>
        /// <returns>
        /// DataSet
        /// </returns>
        public override DataSet First(string table, string fieldKey = "id", int key = 0, string where = "")
        {
            DataSet retValue = new DataSet();

            StringBuilder sQuery = new StringBuilder("SELECT * FROM ");
            sQuery.Append(table);

            if (where.Length > 0)
            {
                sQuery.Append(" WHERE ");
                sQuery.Append(where);
            }

            sQuery.Append(" ORDER BY ");
            sQuery.Append(fieldKey);
            sQuery.Append(" ASC LIMIT 1");

            retValue = this.Execute(sQuery.ToString());

            sQuery = null;

            return retValue;
        }

        /// <summary>
        /// Returns Next Row of Table
        /// </summary>
        /// <param name="table">
        /// Table Name
        /// </param>
        /// <param name="fieldKey">
        /// Field Key
        /// </param>
        /// <param name="key">
        /// Field Key Value
        /// </param>
        /// <param name="where">
        /// Where Statement
        /// </param>
        /// <returns>
        /// DataSet
        /// </returns>
        public override DataSet Next(string table, string fieldKey = "id", int key = 0, string where = "")
        {
            DataSet retValue = new DataSet();

            StringBuilder sQuery = new StringBuilder("SELECT * FROM ");
            sQuery.Append(table);
            sQuery.Append(" WHERE ");
            sQuery.Append(fieldKey);
            sQuery.Append(" > ");
            sQuery.Append(key);

            if (where.Length > 0)
            {
                sQuery.Append(" AND ");
                sQuery.Append(where);
            }

            sQuery.Append(" ORDER BY ");
            sQuery.Append(fieldKey);
            sQuery.Append(" LIMIT 1");

            retValue = this.Execute(sQuery.ToString());

            if (retValue.Tables[0].Rows.Count == 0)
                retValue = this.Last(table, fieldKey, key, where);

            sQuery = null;

            return retValue;
        }

        /// <summary>
        /// Returns Previous Row of Table
        /// </summary>
        /// <param name="table">
        /// Table Name
        /// </param>
        /// <param name="fieldKey">
        /// Field Key
        /// </param>
        /// <param name="key">
        /// Field Key Value
        /// </param>
        /// <param name="where">
        /// Where Statement
        /// </param>
        /// <returns>
        /// DataSet
        /// </returns>
        public override DataSet Previous(string table, string fieldKey = "id", int key = 0, string where = "")
        {
            DataSet retValue = new DataSet();

            StringBuilder sQuery = new StringBuilder("SELECT * FROM ");
            sQuery.Append(table);
            sQuery.Append(" WHERE ");
            sQuery.Append(fieldKey);
            sQuery.Append(" < ");
            sQuery.Append(key);

            if (where.Length > 0)
            {
                sQuery.Append(" AND ");
                sQuery.Append(where);
            }

            sQuery.Append(" ORDER BY ");
            sQuery.Append(fieldKey);
            sQuery.Append(" DESC LIMIT 1");

            retValue = this.Execute(sQuery.ToString());

            if (retValue.Tables[0].Rows.Count == 0)
                retValue = this.First(table, fieldKey, key, where);

            sQuery = null;

            return retValue;
        }

        /// <summary>
        /// Returns Last Row of Table
        /// </summary>
        /// <param name="table">
        /// Table Name
        /// </param>
        /// <param name="fieldKey">
        /// Field Key
        /// </param>
        /// <param name="key">
        /// Field Key Value
        /// </param>
        /// <param name="where">
        /// Where Statement
        /// </param>
        /// <returns>
        /// DataSet
        /// </returns>
        public override DataSet Last(string table, string fieldKey = "id", int key = 0, string where = "")
        {
            DataSet retValue = new DataSet();

            StringBuilder sQuery = new StringBuilder("SELECT * FROM ");
            sQuery.Append(table);

            if (where.Length > 0)
            {
                sQuery.Append(" WHERE ");
                sQuery.Append(where);
            }

            sQuery.Append(" ORDER BY ");
            sQuery.Append(fieldKey);
            sQuery.Append(" DESC LIMIT 1");

            retValue = this.Execute(sQuery.ToString());

            sQuery = null;

            return retValue;
        }

        /// <summary>
        /// Gets a DataSet from Table
        /// </summary>
        /// <param name="table">
        /// Table Name
        /// </param>
        /// <param name="limit">
        /// Limit of Rows
        /// </param>
        /// <returns>
        /// DataSet
        /// </returns>
        public override DataSet Table(string table, int limit = 1000)
        {
            DataSet retValue = new DataSet();

            StringBuilder sQuery = new StringBuilder("SELECT * FROM ");
            sQuery.Append(table);
            sQuery.Append(" LIMIT ");
            sQuery.Append(limit);

            retValue = this.Execute(sQuery.ToString());

            sQuery = null;

            return retValue;
        }

        /// <summary>
        /// Gets a DataSet from Execution of Procedure
        /// </summary>
        /// <param name="procedure">
        /// Stored Procedure Name
        /// </param>
        /// <param name="p">
        /// Parameters to query
        /// </param>
        /// <returns>
        /// DataSet
        /// </returns>
        public override DataSet Procedure(string procedure, params DbParameter[] p)
        {
            MySqlConnection connection = getConnection();

            DataSet myDs = null;

            if (IsConnected())
            {
                try
                {
                    MySqlCommand myCommand = new MySqlCommand();
                    myCommand.Connection = connection;
                    myCommand.CommandText = procedure;
                    myCommand.CommandTimeout = 3600;
                    myCommand.CommandType = CommandType.StoredProcedure;

                    MySqlDataAdapter myAdapter = new MySqlDataAdapter();
                    myDs = new DataSet();

                    for (var i = 0; i < p.Length; i++)
                    {
                        myCommand.Parameters.Add(p[i]);
                    }

                    myAdapter.SelectCommand = myCommand;
                    myAdapter.Fill(myDs);
                }
                catch (Exception e)
                {
                    logger.Error(e.Message);

                    myDs = new DataSet();
                    myDs.Tables.Add(GetErrorTable(-1, e.Message));
                }
            }
            else
            {
                myDs = new DataSet();
                myDs.Tables.Add(GetErrorTable(-2, "Not Connected to MySQL"));
            }

            connection.Close();
            connection = null;

            return myDs;
        }

        /// <summary>
        /// Return Cloned Current Connection Object
        /// </summary>
        /// <returns>
        /// MySqlConnection
        /// </returns>
        private MySqlConnection getConnection()
        {
            return (MySqlConnection)conn.Clone();
        }

        /// <summary>
        /// Gets Row of Table for Id
        /// </summary>
        /// <param name="table">
        /// Table Name
        /// </param>
        /// <param name="fieldKey">
        /// Field Key
        /// </param>
        /// <param name="key">
        /// Field Key
        /// </param>
        /// <returns>
        /// DataSet
        /// </returns>
        public override DataSet TableById(string table, string fieldKey = "id", int key = 0)
        {
            DataSet retValue = new DataSet();

            StringBuilder sQuery = new StringBuilder("SELECT * FROM ");
            sQuery.Append(table);
            sQuery.Append(" WHERE ");
            sQuery.Append(fieldKey);
            sQuery.Append(" = ");
            sQuery.Append(key);

            retValue = this.Execute(sQuery.ToString());

            sQuery = null;

            return retValue;
        }
    }
}