﻿using log4net;
using Nancy;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Web;

// WebModule 
//
// WebModule Web Module Implements NancyModule
//
// Copyright 2016 Jorge Alberto Ponce Turrubiates
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace netWf_IIS
{
    /// <summary>
    /// Web Module
    /// </summary>
    public class WebModule : NancyModule
    {
        /// <summary>
        /// Logger Object.
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(WebModule));

        /// <summary>
        /// Start Web Module
        /// </summary>
        public WebModule()
        {
            // Init netDb
            netDb.GetInstance(netDb.MSSQLSERVER);

            // Validate Key
            Before += ctx =>
            {
                String key = ctx.Request.Query["auth"];
                String path = ctx.Request.Path;

                if (path.StartsWith("/api"))
                {
                    if (key == null)
                        return HttpUtils.ErrorResponse(1);

                    if (!KeyManager.CheckKey(key))
                        return HttpUtils.ErrorResponse(2);
                }

                return null;
            };

            // WebModule Welcome
            Get["/"] = parameters =>
            {
                try
                {
                    return View["index.html"];
                }
                catch (Exception e)
                {
                    return e.Message;
                }
            };

            // Validate Key
            Post["/key"] = parameters =>
            {
                Response response = new Response();

                try
                {
                    string key = Request.Form["key"].Value;

                    if (KeyManager.CheckKey(key))
                        response.StatusCode = HttpStatusCode.OK;
                    else
                        response.StatusCode = HttpStatusCode.NoContent;
                }
                catch (Exception e)
                {
                    logger.Error(e.Message);

                    response = (Response)"Internal Error";
                    response.StatusCode = HttpStatusCode.NotFound;
                }

                return response;
            };

            // Set Key
            Post["/setkey/{key}/{authkey}"] = parameters =>
            {
                Response response = new Response();

                String authKey = parameters.authkey;

                if (!(authKey == System.Configuration.ConfigurationManager.AppSettings["netdb_key"].ToString()))
                    return HttpUtils.ErrorResponse(2);

                try
                {
                    string key = parameters.key;
                    KeyManager.SetKey(key);
                    response.StatusCode = HttpStatusCode.OK;
                }
                catch (Exception e)
                {
                    logger.Error(e.Message);

                    response = (Response)"Internal Error";
                    response.StatusCode = HttpStatusCode.NotFound;
                }

                return response;
            };

            // Gets Table Rows
            Get["/api/table/{name}/{limit:int}"] = parameters =>
            {
                netDb db = netDb.GetInstance();

                DataSet myds = db.Table(parameters.name, parameters.limit);

                return HttpUtils.DataSet2Json(myds);
            };

            // Gets Table Row for Id
            Get["/api/tablebyid/{name}/{fieldkey}/{key:int}"] = parameters =>
            {
                netDb db = netDb.GetInstance();

                DataSet ds = db.TableById(parameters.name, parameters.fieldkey, parameters.key);

                return HttpUtils.DataSet2Json(ds);
            };

            // Gets First Row for Table
            Get["/api/first/{table}/{fieldkey}/{key:int}/{where}"] = parameters =>
            {
                netDb db = netDb.GetInstance();

                string where = parameters.where;
                DataSet ds = new DataSet();

                if (where.Equals("0"))
                    ds = db.First(parameters.table, parameters.fieldkey, parameters.key);
                else
                    ds = db.First(parameters.table, parameters.fieldkey, parameters.key, where);

                return HttpUtils.DataSet2Json(ds);
            };

            // Gets Next Row for Table
            Get["/api/next/{table}/{fieldkey}/{key:int}/{where}"] = parameters =>
            {
                netDb db = netDb.GetInstance();

                string where = parameters.where;
                DataSet ds = new DataSet();

                if (where.Equals("0"))
                    ds = db.Next(parameters.table, parameters.fieldkey, parameters.key);
                else
                    ds = db.Next(parameters.table, parameters.fieldkey, parameters.key, parameters.where);

                return HttpUtils.DataSet2Json(ds);
            };

            // Gets Previous Row for Table
            Get["/api/previous/{table}/{fieldkey}/{key:int}/{where}"] = parameters =>
            {
                netDb db = netDb.GetInstance();

                string where = parameters.where;
                DataSet ds = new DataSet();

                if (where.Equals("0"))
                    ds = db.Previous(parameters.table, parameters.fieldkey, parameters.key);
                else
                    ds = db.Previous(parameters.table, parameters.fieldkey, parameters.key, where);

                return HttpUtils.DataSet2Json(ds);
            };

            // Gets Last Row for Table
            Get["/api/last/{table}/{fieldkey}/{key:int}/{where}"] = parameters =>
            {
                netDb db = netDb.GetInstance();

                string where = parameters.where;
                DataSet ds = new DataSet();

                if (where.Equals("0"))
                    ds = db.Last(parameters.table, parameters.fieldkey, parameters.key);
                else
                    ds = db.Last(parameters.table, parameters.fieldkey, parameters.key, where);

                return HttpUtils.DataSet2Json(ds);
            };

            // Execute Stored Procedure
            Post["/api/procedure/{procedure}"] = parameters =>
            {
                netDb db = netDb.GetInstance();

                DbParameter[] spParameters;
                DataSet ds = new DataSet();
                int i = 0;

                if (Request.Form.Keys.Count > 0)
                {
                    // If sent by Web
                    spParameters = new DbParameter[Request.Form.Keys.Count];

                    foreach (string paramName in Request.Form.Keys)
                    {
                        spParameters[i] = db.GetParameter(paramName, Request.Form[paramName].Value);
                        i++;
                    }
                }
                else
                {
                    // if sent by curl
                    JObject jsonBody = HttpUtils.bodyAsJson(Request.Body);
                    spParameters = new DbParameter[jsonBody.Count];

                    foreach (var item in jsonBody)
                    {
                        string parameter = item.Key;
                        Object value = jsonBody.GetValue(parameter).ToString();

                        spParameters[i] = db.GetParameter(parameter, value);
                        i++;
                    }
                }

                // if sent parameters
                if (i > 0)
                    ds = db.Procedure(parameters.procedure, spParameters);
                else
                    ds = db.Procedure(parameters.procedure);

                return HttpUtils.DataSet2Json(ds);
            };

            // Execute Script
            Post["/api/script/{name}"] = parameters =>
            {
                netDb db = netDb.GetInstance();

                DbParameter[] scParameters;
                DataSet ds = new DataSet();
                int i = 0;

                if (Request.Form.Keys.Count > 0)
                {
                    // If sent by Web
                    scParameters = new DbParameter[Request.Form.Keys.Count];

                    foreach (string paramName in Request.Form.Keys)
                    {
                        scParameters[i] = db.GetParameter(paramName, Request.Form[paramName].Value);
                        i++;
                    }
                }
                else
                {
                    // if sent by curl
                    JObject jsonBody = HttpUtils.bodyAsJson(Request.Body);
                    scParameters = new DbParameter[jsonBody.Count];

                    foreach (var item in jsonBody)
                    {
                        string parameter = item.Key;
                        Object value = jsonBody.GetValue(parameter).ToString();

                        scParameters[i] = db.GetParameter(parameter, value);
                        i++;
                    }
                }

                // Execute Script
                string _currentDir = Environment.CurrentDirectory.Replace("\\", "/");
                string script = _currentDir + "/scripts/" + parameters.name;

                string query = "";

                if (File.Exists(script))
                    query = File.ReadAllText(script);
                else
                    return HttpUtils.ErrorResponse(3);

                if (i > 0)
                    ds = db.Execute(query, scParameters);
                else
                    ds = db.Execute(query);

                return HttpUtils.DataSet2Json(ds);
            };
        }
    }
}