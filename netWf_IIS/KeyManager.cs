﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

// KeyManager 
//
// KeyManager Manage Session Keys
//
// Copyright 2016 Jorge Alberto Ponce Turrubiates
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace netWf_IIS
{
    /// <summary>
    /// Manage Session Keys
    /// </summary>
    public class KeyManager
    {
        /// <summary>
        /// The session keys.
        /// </summary>
        private static Dictionary<string, DateTime> sessionKeys = new Dictionary<string, DateTime>();

        /// <summary>
        /// The expires session days.
        /// </summary>
        private static int expires_days = 1;

        /// <summary>
        /// Adds the default key.
        /// </summary>
        /// <param name="key">Key.</param>
        /// <param name="days">Days to Expire</param>
        public static void AddDefaultKey(string key, int days)
        {
            DateTime dn = DateTime.Now;
            dn = dn.AddDays(days);

            sessionKeys.Add(key, dn);
        }

        /// <summary>
        /// Adds new key.
        /// </summary>
        /// <returns>New key.</returns>
        public static String AddKey()
        {
            String key = GetKey();
            DateTime expires = GetExpiresDate();

            sessionKeys.Add(key, expires);

            return key;
        }

        /// <summary>
        /// Sets new key
        /// </summary>
        /// <param name="NewKey">New Key</param>
        public static void SetKey(string NewKey)
        {
            DateTime expires = GetExpiresDate();

            sessionKeys.Add(NewKey, expires);
        }

        /// <summary>
        /// Removes Session key.
        /// </summary>
        /// <param name="key">Session Key.</param>
        public static void RemoveKey(String key)
        {
            sessionKeys.Remove(key);
        }

        /// <summary>
        /// Checks Session key.
        /// </summary>
        /// <returns>true if session key not expires</returns>
        /// <param name="key">Session Key.</param>
        public static bool CheckKey(String key)
        {
            bool retValue = false;

            if (key != null)
            {
                if (key.Equals(System.Configuration.ConfigurationManager.AppSettings["netdb_key"].ToString()))
                {
                    retValue = true;
                }
                else
                {
                    if (sessionKeys.ContainsKey(key))
                    {
                        DateTime dt = sessionKeys[key];

                        if (DateTime.Compare(dt, DateTime.Now) >= 0)
                            retValue = true;
                    }
                }
            }

            return retValue;
        }

        /// <summary>
        /// Gets New key.
        /// </summary>
        /// <returns>New key.</returns>
        private static String GetKey()
        {
            Guid guid = Guid.NewGuid();
            string id = guid.ToString();
            id = id.Replace("-", "");

            return id;
        }

        /// <summary>
        /// Gets the expires date.
        /// </summary>
        /// <returns>The expires date.</returns>
        private static DateTime GetExpiresDate()
        {
            DateTime dt = DateTime.Now;
            dt = dt.AddDays(expires_days);

            return dt;
        }

    }
}