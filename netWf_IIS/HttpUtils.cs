﻿using log4net;
using Nancy;
using Nancy.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Data;

// HttpUtils 
//
// HttpUtils Utils for Http
//
// Copyright 2016 Jorge Alberto Ponce Turrubiates
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace netWf_IIS
{
    /// <summary>
    /// Nancy Http Utils
    /// </summary>
    public class HttpUtils
    {
        /// <summary>
        /// Logger Object.
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(HttpUtils));

        /// <summary>
        /// Gets MD5 Encrypted String
        /// </summary>
        /// <param name="strMd5">
        /// String to generate MD5
        /// </param>
        /// <returns>
        /// String
        /// </returns>
        public static string MD5(string strMd5)
        {
            System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create();

            byte[] data = md5.ComputeHash(System.Text.Encoding.Default.GetBytes(strMd5));

            System.Text.StringBuilder sBuilder = new System.Text.StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }

        /// <summary>
        /// Redirect Path
        /// </summary>
        /// <param name="strPath">
        /// Path to Redirect
        /// </param>
        /// <returns>
        /// Response
        /// </returns>
        public static Response redirect(string strPath)
        {
            Response response = new Response();
            response.Headers.Add("location", strPath);
            response.ContentType = "text/html";
            response.StatusCode = HttpStatusCode.SeeOther;

            return response;
        }

        /// <summary>
        /// Gets a Json string of DataSet
        /// </summary>
        /// <param name="jsonDs">
        /// Valid DataSet
        /// </param>
        /// <returns>
        /// Json Response
        /// </returns>
        public static Response DataSet2Json(DataSet jsonDs)
        {
            Response response = new Response();

            try
            {
                string jsonStr = JsonConvert.SerializeObject(jsonDs, Formatting.Indented);

                response = (Response)jsonStr;
                response.ContentType = "application/json";
                response.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                response.StatusCode = HttpStatusCode.NotFound;
            }

            return response;
        }

        /// <summary>
        /// Return Error Response validating Key.
        /// </summary>
        /// <returns>Response</returns>
        /// <param name="type">Type. 1 Not Sent Key 2 Permission denied or Invalid Key 3 Script doesn't exists</param>
        public static Response ErrorResponse(int type)
        {
            Response response = new Response();

            try
            {
                string jsonStr = "{\"error\" : \"Not Sent Key\"}";

                if (type == 2)
                    jsonStr = "{\"error\" : \"Permission denied\"}";

                if (type == 3)
                    jsonStr = "{\"error\" : \"Script doesn't exists\"}";

                response = (Response)jsonStr;
                response.ContentType = "application/json";
                response.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                response.StatusCode = HttpStatusCode.NotFound;
            }

            return response;
        }

        /// <summary>
        /// Gets Body Content As JObject
        /// </summary>
        /// <param name="body">
        /// Request Body
        /// </param>
        /// <returns>
        /// JObject
        /// </returns>
        public static JObject bodyAsJson(RequestStream body)
        {
            JObject retValue = new JObject();

            var objBody = body;
            int lenBody = (int)objBody.Length;
            byte[] data = new byte[lenBody];
            objBody.Read(data, 0, lenBody);

            string jsonBody = System.Text.Encoding.Default.GetString(data);

            try
            {
                retValue = JObject.Parse(jsonBody);
            }
            catch (Exception e)
            {
                retValue = new JObject();
            }

            return retValue;
        }

        public static DataTable GetErrorTable(int errorCode, string errormsg)
        {
            DataTable errorTable = new DataTable();

            errorTable.Columns.Add("errorcode", typeof(int));
            errorTable.Columns.Add("errormsg", typeof(string));

            errorTable.Rows.Add(errorCode, errormsg);

            return errorTable;
        }
    }
}