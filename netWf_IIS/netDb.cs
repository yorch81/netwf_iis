﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

namespace netWf_IIS
{
    /// <summary>
    /// Singleton Implementation Class
    /// </summary>
    public class netDb
    {
        /// <summary>
        /// Available Implementations
        /// </summary>
        public const int MSSQLSERVER = 1;
        public const int MYSQL = 2;

        /// <summary>
        /// Singleton Instance
        /// </summary>
        private static netDb INSTANCE = null;

        /// <summary>
        /// Abstract Connection
        /// </summary>
        private absLayer conn = null;

        /// <summary>
        /// Private Constructor
        /// </summary>
        /// <param name="connType">
        /// 1 MSSQLSERVER 2 MYSQL
        /// </param>
        private netDb(int connType)
        {
            // Configure Log File
            //Logger.ConfigureFileAppender("netdb.log");

            switch (connType)
            {
                case 1:
                    conn = new absSqlServer();
                    break;
                case 2:
                    conn = new absMySql();
                    break;
                default:
                    conn = new absSqlServer();
                    break;
            }
        }

        /// <summary>
        /// Singleton Implementation
        /// </summary>
        /// <param name="connType">
        /// 1 MSSQLSERVER 2 MYSQL
        /// </param>
        public static netDb GetInstance(int connType = 1)
        {
            if (INSTANCE == null)
            {
                INSTANCE = new netDb(connType);
            }

            return INSTANCE;
        }

        /// <summary>
        /// Checks if Connected to DataBase Server
        /// </summary>
        /// <returns>
        /// bool
        /// </returns>
        public bool IsConnected()
        {
            return conn.IsConnected();
        }

        /// <summary>
        /// Execute query without result
        /// </summary>
        /// <param name="query">
        /// Query to execute
        /// </param>
        /// <param name="p">
        /// Parameters to query
        /// </param>
        /// <returns>
        /// -2 Not Connected -1 SQL Exception 0 No Results in execution >0 Rows Affected
        /// </returns>
        public int ExecuteNonQuery(string query, params DbParameter[] p)
        {
            return conn.ExecuteNonQuery(query, p);
        }

        /// <summary>
        /// Execute query with result
        /// </summary>
        /// <param name="query">
        /// Query to execute
        /// </param>
        /// <param name="p">
        /// Parameters to query
        /// </param>
        /// <returns>
        /// Dataset
        /// </returns>
        public DataSet Execute(string query, params DbParameter[] p)
        {
            return conn.Execute(query, p);
        }

        /// <summary>
        /// Returns a Parameter
        /// </summary>
        /// <param name="name">
        /// Parameter Name
        /// </param>
        /// <param name="value">
        /// Parameter Value
        /// </param>
        /// <returns>
        /// DbParameter
        /// </returns>
        /// <remarks>
        /// For ODBC the parameters in query must be ? 
        /// and the parameter name does not matter
        /// </remarks>
        public DbParameter GetParameter(string name, object value)
        {
            return conn.GetParameter(name, value);
        }

        /// <summary>
        /// Returns First Row of Table
        /// </summary>
        /// <param name="table">
        /// Table Name
        /// </param>
        /// <param name="fieldKey">
        /// Field Key
        /// </param>
        /// <param name="key">
        /// Field Key Value
        /// </param>
        /// <param name="where">
        /// Where Statement
        /// </param>
        /// <returns>
        /// DataSet
        /// </returns>
        public DataSet First(string table, string fieldKey = "id", int key = 0, string where = "")
        {
            return conn.First(table, fieldKey, key, where);
        }

        /// <summary>
        /// Returns Next Row of Table
        /// </summary>
        /// <param name="table">
        /// Table Name
        /// </param>
        /// <param name="fieldKey">
        /// Field Key
        /// </param>
        /// <param name="key">
        /// Field Key Value
        /// </param>
        /// <param name="where">
        /// Where Statement
        /// </param>
        /// <returns>
        /// DataSet
        /// </returns>
        public DataSet Next(string table, string fieldKey = "id", int key = 0, string where = "")
        {
            return conn.Next(table, fieldKey, key, where);
        }

        /// <summary>
        /// Returns Previous Row of Table
        /// </summary>
        /// <param name="table">
        /// Table Name
        /// </param>
        /// <param name="fieldKey">
        /// Field Key
        /// </param>
        /// <param name="key">
        /// Field Key Value
        /// </param>
        /// <param name="where">
        /// Where Statement
        /// </param>
        /// <returns>
        /// DataSet
        /// </returns>
        public DataSet Previous(string table, string fieldKey = "id", int key = 0, string where = "")
        {
            return conn.Previous(table, fieldKey, key, where);
        }

        /// <summary>
        /// Returns Last Row of Table
        /// </summary>
        /// <param name="table">
        /// Table Name
        /// </param>
        /// <param name="fieldKey">
        /// Field Key
        /// </param>
        /// <param name="key">
        /// Field Key Value
        /// </param>
        /// <param name="where">
        /// Where Statement
        /// </param>
        /// <returns>
        /// DataSet
        /// </returns>
        public DataSet Last(string table, string fieldKey = "id", int key = 0, string where = "")
        {
            return conn.Last(table, fieldKey, key, where);
        }

        /// <summary>
        /// Gets a DataSet from Table
        /// </summary>
        /// <param name="table">
        /// Table Name
        /// </param>
        /// <param name="limit">
        /// Limit of Rows
        /// </param>
        /// <returns>
        /// DataSet
        /// </returns>
        public DataSet Table(string table, int limit = 1000)
        {
            return conn.Table(table, limit);
        }

        /// <summary>
        /// Gets a DataSet from Execution of Procedure
        /// </summary>
        /// <param name="procedure">
        /// Stored Procedure Name
        /// </param>
        /// <param name="p">
        /// Parameters to query
        /// </param>
        /// <returns>
        /// DataSet
        /// </returns>
        public DataSet Procedure(string procedure, params DbParameter[] p)
        {
            return conn.Procedure(procedure, p);
        }

        /// <summary>
        /// Gets Row of Table for Id
        /// </summary>
        /// <param name="table">
        /// Table Name
        /// </param>
        /// <param name="fieldKey">
        /// Field Key
        /// </param>
        /// <param name="key">
        /// Field Key
        /// </param>
        /// <returns>
        /// DataSet
        /// </returns>
        public DataSet TableById(string table, string fieldKey = "id", int key = 0)
        {
            return conn.TableById(table, fieldKey, key);
        }
    }
}