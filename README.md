# netDb Web Framework #

## Description ##
netDb Web Framework on IIS.

## Requirements ##
* [.NET Framework](http://www.microsoft.com/es-mx/download/details.aspx?id=30653)
* [NancyFx](http://nancyfx.org/)
* [SQL Server](http://www.microsoft.com/es-es/server-cloud/products/sql-server/)
* [MySQL](http://www.mysql.com/)
* [ASP.NET](http://www.asp.net/)

## Developer Documentation ##
In the Code.

## Installation ##
Clone project

## REST Api ##
Rows number by Table:
$ curl http://MYURL:PORT/api/table/TABLE_NAME/ROWS?auth=MASTER_KEY

One row by Id:
$ curl http://MYURL:PORT/api/tablebyid/TABLE_NAME/FIELD/KEY?auth=MASTER_KEY

First row:
$ curl http://MYURL:PORT/api/first/TABLE_NAME/FIELD/KEY/WHERE_CLAUSE?auth=MASTER_KEY

Next row:
$ curl http://MYURL:PORT/api/next/TABLE_NAME/FIELD/KEY/WHERE_CLAUSE?auth=MASTER_KEY

Previous row:
$ curl http://MYURL:PORT/api/previous/TABLE_NAME/FIELD/KEY/WHERE_CLAUSE?auth=MASTER_KEY

Last row:
$ curl http://MYURL:PORT/api/last/TABLE_NAME/FIELD/KEY/WHERE_CLAUSE?auth=MASTER_KEY

Execute Stored Procedure:
$ curl -H "Content-Type: application/json" -X POST -d JSON_PARAMETERS http://MYURL:PORT/api/procedure/PROCEDURE_NAME?auth=MASTER_KEY

Execute SQL Script:
$ curl -H "Content-Type: application/json" -X POST -d JSON_PARAMETERS http://MYURL:PORT/api/script/SCRIPT_NAME?auth=MASTER_KEY

## References ##
https://es.wikipedia.org/wiki/Representational_State_Transfer